from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.


class CustomUser(AbstractUser):
	
    REQUIRED_FIELDS = ['name','email']

    name = models.CharField(max_length=50,help_text='Name')

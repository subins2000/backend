from django.db import models


class Camp(models.Model):
    name = models.CharField(
    max_length=20,
    help_text='Name',
    )
    location = models.CharField(max_length=200, help_text='Location name')
    latitude = models.CharField(
        max_length=50,
        help_text='Latitude',
    )
    longitude = models.CharField(
        max_length=50,
        help_text='Longitude',
    )
    contact_info = models.CharField(
        max_length=50,
        help_text='Contact_info',
    )
    incharge = models.CharField(
        max_length=40,
        help_text='Camp incharge',
    )
    phone = models.CharField(
        max_length=15,
        help_text='Camp phone number'
    )
    photo = models.ImageField(
        blank=True,
        null=True,
        upload_to='camps/%Y/%m/%d/'
    )
    capacity = models.IntegerField(
        help_text='The max capacity of the camp'
    )
    number_of_people = models.IntegerField(
        default=0,
        help_text='The number of people currently in the camp'
    )

    def __str__(self):
        return "{}-{}".format(self.name,self.location)



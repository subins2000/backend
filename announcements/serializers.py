from rest_framework import serializers
from user_accounts.serializers import UserSerializer
from announcements.models import Announcement


class AnnouncementSerializer(serializers.ModelSerializer):

    user = serializers.SerializerMethodField()

    class Meta:
        model = Announcement
        fields = ['user','title','contents','hash_tags']


    def get_user(self,obj):
        user = obj.user
        serializer = UserSerializer(user)
        return serializer.data
